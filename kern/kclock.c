/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <kern/kclock.h>
#include <inc/stdio.h>
#include <kern/env.h>
#include <kern/monitor.h>

void
rtc_init(void)
{
	nmi_disable();
	// LAB 4: your code here
	// task 1
	// My code starts
	outb(IO_RTC_CMND, RTC_BREG);
	outb(IO_RTC_DATA, inb(IO_RTC_DATA) | RTC_PIE);

	outb(IO_RTC_CMND, RTC_AREG);
	outb(IO_RTC_DATA, inb(IO_RTC_DATA) | 0xF); // period is set to half a second
	// My code ends

	nmi_enable();
}

uint8_t
rtc_check_status(void)
{
	/*[>uint8_t status = 0;<]*/
	// LAB 4: your code here
	// task 1
	// My code starts
	outb(IO_RTC_CMND, RTC_CREG);
	return inb(IO_RTC_DATA);
	// My code ends

	/*[>return status;<]*/
}

