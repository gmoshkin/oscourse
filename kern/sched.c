#include <inc/assert.h>
#include <inc/x86.h>
#include <kern/env.h>
#include <kern/monitor.h>


struct Taskstate cpu_ts;
void sched_halt(void);

// Choose a user environment to run and run it.
void
sched_yield(void)
{
	struct Env *idle;

	// Implement simple round-robin scheduling.
	//
	// Search through 'envs' for an ENV_RUNNABLE environment in
	// circular fashion starting just after the env was
	// last running.  Switch to the first such environment found.
	//
	// If no envs are runnable, but the environment previously
	// running is still ENV_RUNNING, it's okay to
	// choose that environment.
	//
	// If there are no runnable environments,
	// simply drop through to the code
	// below to halt the cpu.

	// LAB 3: Your code here.
	// task 3

	int lr_idx = 0; // index of the last running environment
	int i = 0;
	if (curenv) {
		lr_idx = ENVX(curenv->env_id);
		i = 1;
	}
	for (; i < NENV; ++i) { // looking for a runnable environment for a single loop
		/*if (i == lr_idx)*/
			/*continue;*/
		if (envs[(lr_idx + i) % NENV].env_status == ENV_RUNNABLE) {
			cprintf("envrun RUNNABLE: %d\n", (lr_idx + i) % NENV);
			env_run(&envs[(lr_idx + i) % NENV]);
		}
	}
	/*cprintf("Debug: sched_yield: end of loop\n");*/
	if (curenv->env_status == ENV_RUNNING) { // run the privious running environment
		cprintf("envrun RUNNING: %d\n", lr_idx);
		env_run(curenv);
	}
	cprintf("sched_yield: nothing to run, so I'm just gonna halt for a while...\n");

// 	env_run(&envs[0]);

	// My code ends here

	// sched_halt never returns
	sched_halt();
}

// Halt this CPU when there is nothing to do. Wait until the
// timer interrupt wakes it up. This function never returns.
//
void
sched_halt(void)
{
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
		cprintf("sched_halt:loop\n");
		if ((envs[i].env_status == ENV_RUNNABLE ||
		     envs[i].env_status == ENV_RUNNING))
			break;
	}
	if (i == NENV) {
		cprintf("No runnable environments in the system!\n");
		while (1)
			monitor(NULL);
	}

	// Mark that no environment is running on CPU
	curenv = NULL;

	// Reset stack pointer, enable interrupts and then halt.
	asm volatile (
		"movl $0, %%ebp\n"
		"movl %0, %%esp\n"
		"pushl $0\n"
		"pushl $0\n"
		"sti\n"
		"hlt\n"
	: : "a" (cpu_ts.ts_esp0));
}

